
FROM registry.gitlab.com/media-cloud-ai/workers/generic/rs_command_line_worker:0.3.2-debian as command_line
FROM mediacloudai/docker_alpine_ffmpeg:buster-latest as ffmpeg

COPY --from=command_line /usr/bin/command_line_worker /usr/bin/command_line_worker

CMD ["command_line_worker"]
